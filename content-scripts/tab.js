const gettingItem = browser.storage.sync.get("port");
const connectionStatus = document.getElementById("connection-status");
let ws;
const id = Date.now() % 1000;
console.log("ID:", id);
let activeElement;
let innerText;

gettingItem.then((res) => {
  const port = res.port || "40000";
  ws = new WebSocket("ws://127.0.0.1:" + port);
  ws.onopen = () => {
    ws.send(
      JSON.stringify({
        type: "open",
        connection: "tab",
        id,
      })
    );
  };
  ws.onmessage = (message) => {
    const content = JSON.parse(message.data);
    switch (content.type) {
      case "eval":
        eval(content.body);
        break;
      case "request-edit":
        activeElement = document.activeElement;
        innerText = !activeElement.value;
        ws.send(
          JSON.stringify({
            type: "response",
            "request-id": content["request-id"],
            text: innerText ? activeElement.innerText : activeElement.value,
          })
        );
        break;
      case "set-edit":
        if (innerText) {
          activeElement.innerText = content.body;
        } else {
          activeElement.value = content.body;
        }
        break;
    }
  };
  ws.onclose = () => {
    console.log("Close");
  };
});

function updateTitle(title) {
  console.log(title);
  if (!title.startsWith("(:title")) {
    document.title =
      "(:title " +
      JSON.stringify(document.title) +
      " :id " +
      JSON.stringify(id) +
      ")";
  }
}

new MutationObserver(function (mutations) {
  const title = mutations[0].target.innerText;
  updateTitle(title);
}).observe(document.querySelector("title"), {
  subtree: true,
  characterData: true,
  childList: true,
});

updateTitle(document.title);
