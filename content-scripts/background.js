const gettingItem = browser.storage.sync.get("port");
const connectionStatus = document.getElementById("connection-status");
let ws;

gettingItem.then((res) => {
  const port = res.port || "40000";
  ws = new WebSocket("ws://127.0.0.1:" + port);

  ws.onopen = () => {
    ws.send(
      JSON.stringify({
        type: "open",
        connection: "background",
      })
    );
  };

  ws.onmessage = (message) => {
    const content = JSON.parse(message.data);
    if (content.type === "set-theme") {
      browser.theme.update(content.theme);
    }
  };
});
