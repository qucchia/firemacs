function saveOptions(e) {
  browser.storage.sync.set({
    port: document.querySelector("#port").value,
  });
  e.preventDefault();
}

function restoreOptions() {
  let gettingItem = browser.storage.sync.get("port");
  gettingItem.then((res) => {
    document.querySelector("#port").value = res.port || "40000";
  });
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
