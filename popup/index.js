const gettingItem = browser.storage.sync.get("port");
const connectionStatus = document.getElementById("connection-status");
let ws;

gettingItem.then((res) => {
  const port = res.port || "40000";
  ws = new WebSocket("ws://127.0.0.1:" + port);
  ws.onopen = () => {
    connectionStatus.classList.add("success");
    connectionStatus.innerText = "Connected";
  };
  ws.onerror = () => {
    connectionStatus.classList.add("failure");
    connectionStatus.innerText = "Not connected";
  };
  ws.onmessage = (message) => {
    const content = JSON.parse(message.data);
    if (content.type === "eval") {
      eval(content.body);
    }
  };
});

document.getElementById("post").onclick = () => {
  ws.send(JSON.stringify({ a: 3 }));
};
